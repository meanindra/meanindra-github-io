---
layout: post
current: post
cover:  assets/images/saad-chaudhry-cYpqYxGeqts-unsplash.jpg
navigation: True
title: Top nature description in this world
date: 2019-07-27 10:00:00
tags: [Getting started]
class: post-template
subclass: 'post'
author: ghost
---

Nature is a British weekly scientific journal founded and based in London, England. As a multidisciplinary publication, Nature features peer-reviewed research from a variety of academic disciplines, mainly in science, technology, and the natural sciences. It has core editorial offices across the United States, continental Europe, and Asia under the international scientific publishing company Springer Nature. Nature was one of the world's most cited scientific journals by the Science Edition of the 2019 Journal Citation Reports (with an ascribed impact factor of 42.778),[1] making it one of the world's most-read and most prestigious academic journals.[2][3][4] As of 2012, it claimed an online readership of about three million unique readers per month.[5]

Founded in Autumn 1869, Nature was first circulated by Norman Lockyer and Alexander Macmillan as a public forum for scientific innovations. The mid-20th century facilitated an editorial expansion for the journal; Nature redoubled its efforts in explanatory and scientific journalism. The late-1980s and early-1990s created a network of editorial offices outside of Britain and established ten new supplementary, specialty publications (e.g. Nature Materials). Since the late 2000s, the weekly created dedicated editorial and current affairs columns, as well as electoral endorsements. The primary source of the journal remain, as established at its founding, research scientists; editing standards are primarily concerned with technical readability. Each issue also features articles that are of general interest to the scientific community, namely business, funding, scientific ethics and research breakthroughs. There are also sections on books, arts, and short science fiction stories.

The main research published in Nature consists mostly of papers (articles or letters) in lightly-edited form. As such they are highly technical and dense, but due to imposed text limits they are typically summaries of larger work. Innovations or breakthroughs in any scientific or technological field is featured in the journal as either letters or news articles. The papers that have been published in this journal are internationally acclaimed for maintaining high research standards. Conversely, due to journal's exposure, it has, at various times, been the subject of controversy for its handling of academic dishonesty, the scientific method, and news coverage. Fewer than 8% of submitted papers are accepted for publication.[6] In 2007 Nature (together with Science) received the Prince of Asturias Award for Communications and Humanity.[7][8]


Ghost is made by an independent non-profit organisation called the Ghost Foundation. We are 100% self funded by revenue from our [Ghost(Pro)](https://ghost.org/pricing) service, and every penny we make is re-invested into funding further development of free, open source technology for modern journalism.

The main thing you'll want to read about next is probably: [the Ghost editor](https://demo.ghost.io/the-editor/).

Once you're done reading, you can simply delete the default **Ghost** user from your team to remove all of these introductory posts!
